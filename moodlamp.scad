$fn=150;
translate([2,-2,02]) union(){
	difference(){
		translate([-2,2,26]) cube(size=[75,104, 10]);
		translate([35, 47.27, 25])cylinder(h=12, r=47.27);
		translate([71,2,26])cube(size=[2,2,4]);
		translate([-2,2,26])cube(size=[2,2,4]);
		translate([-2,104,26])cube(size=[2,2,4]);
		translate([71,104,26])cube(size=[2,2,4]);
	}
	translate([-2,0,0])difference(){
		union(){
			translate([37, 47.27, 26]) difference(){
				cylinder(h=25, r=50);
				translate([0,0,1])cylinder(h=27, r=47.27);
			}
		}
		translate([2,4,26]) cube(size=[71,104, 10]);
	}
}


//translate([0,0,8])union(){
union(){
	translate([0,0, 30]) difference(){
		translate([0, 0, -40]) difference(){
			cube(size=[75, 104, 30]);
			translate([2,2,6]) cube(size=[71,100, 26]);
	
		}
	}		
	translate([73,102,20]) cube(size=[2,2,4]);
	translate([0,102,20]) cube(size=[2,2,4]);
	translate([0,0,20]) cube(size=[2,2,4]);
	translate([73,0,20]) cube(size=[2,2,4]);
}