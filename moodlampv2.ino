byte ledG=5;
byte ledR=3;
byte ledB=6;
byte currG=255;
byte currR=255;
byte currB=255;
byte current[3]={currR,currG,currB};
double colorTime = 180000;

void setup(){
  Serial.begin(9600);
  pinMode(ledR, OUTPUT);
  pinMode(ledG, OUTPUT);
  pinMode(ledB, OUTPUT);
  randomSeed(analogRead(0));
}

void loop(){
  byte newR=(unsigned byte) random() % 255;
  byte newG=(unsigned byte) random() % 255;
  byte newB=(unsigned byte) random() % 255;
  
  changeMood(newR, current[0], newG, current[1], newB, current[2], ledR, ledB, ledG);
  delay(colorTime);
}
  byte changeMood(byte newR, byte currR, byte newG, byte currG, byte newB, byte currB, byte ledR, byte ledB, byte ledG){
    for(int i=0; i<max(abs(currR-(currR-newR)),max(abs(currG-(currG-newG)),abs(currB-(currB-newB))));i++){
    if(currR > newR){
      analogWrite(ledR, currR-1);
      currR = currR-1;
    }else if(currR < newR){
      analogWrite(ledR, currR+1);
      currR = currR+1;
      current[0]=currR;
    }
    if(currG > newG){
      analogWrite(ledG, currG-1);
      currG = currG-1;
    }else if(currG < newG){
      analogWrite(ledG, currG+1);
      currG = currG+1;
    }
    if(currB > newB){
      analogWrite(ledB, currB-1);
      currB = currB-1;
    }else if(currB < newB){
      analogWrite(ledB, currB+1);
      currB = currB+1;
    }
    delay(10);
  }
  current[0]=currR;
  current[1]=currG;
  current[2]=currB;
  return current[0,1,2];
}
